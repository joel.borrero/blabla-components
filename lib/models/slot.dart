import 'package:blabla_compoments/constants.dart';
import 'package:flutter/material.dart';

class Slot {
  final DateTime start;
  final bool available;
  final Color color;
  final bool solid;
  bool selected = false;

  Slot({
    required this.start,
    this.available = true,
    this.color = AppColors.primaryColor,
    this.solid = true,
  });
}
