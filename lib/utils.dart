String getDayName(DateTime date, {bool plural = false, short = false}) {
  // Jueves || jue
  String response = '';
  switch (date.weekday) {
    case 1:
      response = 'Lunes';
      break;
    case 2:
      response = 'Martes';
      break;
    case 3:
      response = 'Miércoles';
      break;
    case 4:
      response = 'Jueves';
      break;
    case 5:
      response = 'Viernes';
      break;
    case 6:
      response = 'Sábado${plural ? 's' : ''}';
      break;
    case 7:
      response = 'Domingo${plural ? 's' : ''}';
      break;
    default:
      response = '';
  }
  if (short) {
    response = response.substring(0, 3).toLowerCase();
  }
  return response;
}
