import 'package:flutter/material.dart';

import 'package:patterns_canvas/patterns_canvas.dart';
import 'package:time_planner/time_planner.dart';

import 'package:blabla_compoments/constants.dart';
import 'package:blabla_compoments/models/slot.dart';
import 'package:blabla_compoments/utils.dart';

class Componente3 extends StatefulWidget {
  const Componente3({
    Key? key,
    required this.slots,
    required this.selectedSlots,
  }) : super(key: key);

  final List<Slot> slots;
  final List<Slot> selectedSlots;

  @override
  State<Componente3> createState() => _Componente3State();
}

class _Componente3State extends State<Componente3> {
  final List<TimePlannerTask> reservations = [];
  final List<Slot> selectedSlots = [];
  @override
  Widget build(BuildContext context) {
    final DateTime todayFlat = DateTime.now().copyWith(
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
      microsecond: 0,
    );

    reservations.clear();
    for (final Slot slot in widget.slots) {
      if (slot.start.compareTo(todayFlat) >= 0) {
        reservations.add(
          TimePlannerTask(
            minutesDuration: 30,
            dateTime: TimePlannerDateTime(
              day: slot.start.difference(todayFlat).inDays,
              hour: slot.start.hour,
              minutes: slot.start.minute,
            ),
            color: Colors.transparent,
            onTap: () {
              if (!slot.available) return;

              if (slot.selected) {
                slot.selected = false;
                widget.selectedSlots.remove(slot);
              } else {
                slot.selected = true;
                widget.selectedSlots.add(slot);
              }

              setState(() {});
            },
            child: CustomPaint(
              painter: MyPainter(
                color: widget.selectedSlots.contains(slot)
                    ? Colors.green
                    : slot.color,
                solid: slot.solid,
              ),
            ),
          ),
        );
      }
    }
    return Scaffold(
      appBar:
          AppBar(title: Text('Componente 3 (${widget.selectedSlots.length})')),
      body: TimePlanner(
        startHour: 0,
        endHour: 23,
        headers: List.generate(
          31,
          (index) {
            List<String> dateHeader = getDateHeader(todayFlat, index);
            TextStyle textStyle = TextStyle(
              color: index == 0
                  ? AppColors.primaryColor
                  : Colors.black,
              fontWeight: FontWeight.bold,
            );
            return TimePlannerTitle(
              date: dateHeader[0],
              title: dateHeader[1],
              titleStyle: textStyle,
              dateStyle: textStyle,
            );
          },
        ),
        tasks: reservations,
        style: TimePlannerStyle(
          cellHeight: 80,
          cellWidth: 80,
          dividerColor: Colors.white,
          borderRadius: BorderRadius.circular(0),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  final Color color;
  final bool solid;
  MyPainter({
    required this.color,
    required this.solid,
  });

  @override
  void paint(Canvas canvas, Size size) {
    const rect = Rect.fromLTWH(-40, -20, 80, 40);
    Pattern pattern = DiagonalStripesLight(
      bgColor: Colors.transparent,
      fgColor: color,
      featuresCount: 30,
    );
    solid
        ? canvas.drawRect(rect, Paint()..color = color)
        : pattern.paintOnRect(canvas, size, rect);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

List<String> getDateHeader(DateTime today, int index) {
  DateTime day = today.add(Duration(days: index));
  String dateHeader = getDayName(day, short: true);
  return [day.day.toString(), dateHeader];
}
