import 'package:flutter/material.dart';

import 'components/componente_3.dart';
import 'components/home.dart';
import 'models/slot.dart';

void main() => runApp(const MyApp());

final List<Slot> _selectedSlots = [];

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    return MaterialApp(
      title: 'BlaBla Components',
      home: Navigator(
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case '/':
              return MaterialPageRoute(builder: (context) => const Home());
            case '/componente3':
              return MaterialPageRoute(
                builder: (context) => Componente3(
                  selectedSlots: _selectedSlots,
                  slots: [
                    Slot(start: now),
                    Slot(
                      start: now.add(const Duration(minutes: 30)),
                      color: Colors.teal,
                      available: false,
                    ),
                    Slot(
                      start: now.add(const Duration(hours: 2)),
                      solid: false,
                    ),
                    Slot(
                      start: now.add(const Duration(days: 1, hours: 2)),
                      color: Colors.black,
                      solid: false,
                    ),
                  ],
                ),
              );
            default:
              return MaterialPageRoute(builder: (context) => const Home());
          }
        },
      ),
    );
  }
}
